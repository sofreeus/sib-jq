LAB 3 Play with JQ pt II
------------------------

1) Gather information from the 'intro to devops' project

  - gather only the following fields: ssh_url_to_repo and the name with the namespace?
  - show in the MM classroom how you got those
  - take those 2 values, and put them in a new file called new.json

curl --silent -X GET https://gitlab.com/api/v4/projects/sofreeus%2Fintrotodevops

*2) Re-format objects in 'intro to devops' project, and give the key (aka name) for ssh_url_to_repo a new name of RepoSSH.

*3) Find your GitLab user id, and gather your user settings via API

4) Break apart project array, and get all the 'names'

5) With projects array, return only the 7th project. What project is that?

6) Ammed your command from #5, and now drill down to get only the following values from the 7th project:
   merge requests and issues links

curl --silent -X GET https://gitlab.com/api/v4/groups/sofreeus

* = optional
