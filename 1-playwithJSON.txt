LAB 1 - play with JSON
----------------------

1) Create JSON file containing 3 objects.
   Object 0 is the neighbor on your LEFT
   Object 1 is yourself
   Object 2 is the neighbor on your RIGHT
  
   Each object contains the same 3 fields (name/value pairs):
   first name, age, hometown
   
   Save the file
   

2) 'cat' the file and pipe it through jq (install jq if need be)


3) Alter that JSON file so that the 3 objects are within an array,
   and cat | jq again.
   
   Feel free to post results to MM Classroom
   
   NOTE: jq isn't a 'linter', but notice how it can give helpful error output on malformed JSON