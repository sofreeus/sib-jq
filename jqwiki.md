# __SFS__  - 03/16/2019 - "JQ, JSON & Data Serialization"
_Rich Glazier,  glazier@gmail.com,  RMSday_  
_git clone git@bitbucket.org:sofreeus/jq-and-data-serialization.git_

.
## Overview of: 

## 1) Data Serialization in general 
## 2) JSON format specifically 
## 3) API standards 
## 4) Using cURL to a GET API queries in JSON
## 5) Using JQ to "pretty print" and to parse JSON.
## 6) Using JQ to get just what you want.

.
On your own, investigate

   > Linting and XMLlint

   > Python json module


----------
## 1) Data Serialization

_WHAT?_


   A way to convert complex objects to into a human readable stream or sequence.  
   Let the machines talk, but in a non-machine way.



_WHY?_


   For data interchange.  
   One way for a human or program to glean information from a Web Site.  
   A way to PUT information in a datastore.  
   To have a standard way that all applications, appliances, developers, and consumers can speak.  


_HOW?_

   Reading application documentation and lot's of DDG and StackOverflow.  
   Every place you may stream data via a Data Serialization format can be vastly different (eg, API framework, their specific meaning of HTTP methods, multiple API versions, security, supporting more than one Data Serialization format, etc.)

_WHAT it's __NOT___

A database


https://en.wikipedia.org/wiki/Comparison_of_data_serialization_formats (note that some are markup as well, and some aren't human readable) 

https://en.wikipedia.org/wiki/Serialization

----------
## 2) JSON

_WHAT?_

- A format for serializing data  
- Data representation. 
_J_ ava _S_ cript _O_ bject _N_ otation- As the name might imply, it was primarily used, and is currently heavily used, to 'serialize' Java Objects. The __Java Script__ part, might indicate to you that it's often used to pass data to and from Web front-end applications.



_WHY?_

- programming language agnostic


- a standard for program communication  
        ex. Backend Sever to Web-tier communication in JSON format, Websockets
- a syntax that easily feeds into Java, JS, Node.JS_ (ie. web pages)

    _Web frontends are not static, and are expected to have dynamic web content, these days. This Web UI is BitBucket, and my interaction with it implies adding, removing, and editing data. This data doesn't live on the web page, but data must get pulled from a datasore and presented to my particular page.. In what way is it transferring data between the back-end and he front-end?_

_HOW_?

- Interact programmatically with a Web Site  
- Write date into a filesystem
- IDEs and tools that understand JSON

_JSON BREAKDOWN_  

- _Object tree_, consisting of objects such as _strings_, _numbers_, _arrays_, and other objects, that are in a blocked and nested format similar to Java (such as brackets and curly braces).  
- JSON objects are contained within ```{}```  
- Field names within JSON objects are contained within ```""``` and end with a ```:```  
- Name values can be strings, integers, arrays, nested objects, and other types.  
- Arrays are contained within ```[]```  
- ```{}``` delimit code blocks or objects, and so leading white-space doesn't matter

_WHAT it's __NOT___

- A database
- A programming language
- A transport protocol

_JSON GENERATOR_  

https://jsonplaceholder.typicode.com/
http://jeremydorn.com/json-editor/


_JSON SYNTAX_  

https://restfulapi.net/json-syntax/


_JSON PRIMER AND JQ_
https://programminghistorian.org/en/lessons/json-and-jq


----------
## 3) APIs

_WHAT_?

- _A_ pplication _P_ rogramming _I_ nterface
- A means for non-programmers to access the data.  
  Uncompiled, not a query language, human readable

_WHY?_

- Third party development
- Programmatic Automation
- PULL and PUSH data between on Prem Apps/Webservers
- PULL and PUSH data between on-Prem and cloud
- Just about every programming lang has a way of 'speaking' HTTP, and therefore a way to utilize Web APIs. 
- Access to data you might not have access to via the Web UI
- A software 'value-add'
- The 'rules of engagement'

_HOW?_

- HTTP requests (RPC)  
        ex. _a PUT command to populate fields on a web site, and send the data in a serialized format that the web site can understand, like XML or JSON_

- SOAP
- REST
- Programming modules
- IDEs
- cURL

_WHAT it's __NOT___

- Easy, although it's often billed as such
- Not necessarily secure
- Often not well documented

https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding

----------
## 4) cURL

_WHAT_?
 
- A command line utility to interact via HTTP

_WHY?_ 

- Common way to fetch data via an API.  
- Call from a script to PUSH or PULL HTTP data via and API

_HOW?_

- ```man curl```
- Can specify a data serialization format like JSON or XML, or API version. 
- Can pass clear text API login credentials, or pass from a protected file. 

_WHAT IT'S NOT_

- A hot cup of coffee 

_SOFREE URL__

curl --silent -X GET https://gitlab.com/api/v4/projects/sofreeus%2Fintrotodevops

----------
## 5) JQ

_WHAT_? 

- JSON Query utility  

_WHY?_ 

- Some programming languages have a lib./module or built-in way to parse JSON, or to flatten JSON into key-value pairs that can be digested, but Bash does not, so you would ```|``` to jq for this.  
- 'Pretty Print' JSON. 
- Parse JSON, to get at specific objects or values.  
- Great documentation 

_HOW?_

- use ```jq``` in Bash scripts, to more easily parse data.  
- use ```jq``` to take JSON stream in a DB, CSV, or file.  
- use ```jq``` on the command line to make JSON output easier to read.  

_REALLY HOW_  

. is like / is file-system
you drill down objects (keys, nested objects, arrays, etc), with '.'


_ODDITIES_

- you can't pipe jq output to other Bash commands. See this link for more information.
  https://github.com/stedolan/jq/issues/1496
  One work-around, is to redirect to file, and then act upon that file output, rather than raw ```jq``` output. Install ```sponge```.

_JQ MANUAL_

https://stedolan.github.io/jq/manual/#Basicfilters

_GETTING FAMILIAR WITH JQ_  

https://restfulapi.net/json-syntax/  
https://stedolan.github.io/jq/tutorial/  
https://thoughtbot.com/blog/jq-is-sed-for-json  
https://jqplay.org/ 

_MORE ADVANCED JQ EXAMPLES_

https://github.com/stedolan/jq/wiki/Cookbook
